# Softex test task 

Api documentation located in api_docs.apib. [Can be watched with https://apiary.io](https://apiary.io)

##Installation

- Copy .env.example to .env
```bash
php -r \"file_exists('.env') || copy('.env.example', '.env');\"
```
- Fill database fields and set MAILCHIMP_APIKEY
- Run package installation
```bash
composer install
```
- Migrate database
```bash
php artisan migrate:fresh
```
- Seed database if you want to fetch existing lists from MailChimp 
```bash
php artisan db:seed
```
- You are awesome!

