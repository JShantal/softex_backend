<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Facade;

class MailChimpHelperFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'mailchimp_helper'; }
}
