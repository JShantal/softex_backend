<?php

namespace App\Helpers;

use DrewM\MailChimp\MailChimp;

class MailChimpHelper
{
    /** @var MailChimp */
    protected $mailChimp;

    /**
     * MailChimpHelper constructor.
     *
     * @param MailChimp $mailChimp
     */
    public function __construct(MailChimp $mailChimp)
    {
        $this->mailChimp = $mailChimp;
    }

    /**
     * Show MailChimp lists
     *
     * @return array
     */
    public function getLists()
    {
        return $this->mailChimp->get("lists");
    }

    /**
     * Create MailChimp list
     *
     * @param array $list
     * @return string
     */
    public function createList(array $list)
    {
        $list = $this->mailChimp->post("lists", $list);
        return $list['id'];
    }

    /**
     * Show MailChimp list
     *
     * @param string $id
     * @return array
     */
    public function showList(string $id)
    {
        return $this->mailChimp->get("lists/$id");
    }

    /**
     * Update MailChimp list
     *
     * @param string $id
     * @param array $list
     * @return boolean
     */
    public function updateList(string $id, array $list)
    {
        unset($list['id']);
        return $this->mailChimp->patch("lists/$id", $list);
    }

    /**
     * Delete MailChimp list
     *
     * @param string $id
     * @return boolean
     */
    public function deleteList(string $id)
    {
        return $this->mailChimp->delete("lists/$id");
    }

    /**
     * Show MailChimp list members
     *
     * @return array
     */
    public function getMembers(string $list)
    {
        return $this->mailChimp->get("lists/$list/members");
    }

    /**
     * Create MailChimp list member
     *
     * @param string $list
     * @param array $member
     * @return string
     */
    public function createMember(string $list, array $member)
    {
        unset($member['list_id']);
        $member = $this->mailChimp->post("lists/$list/members", $member);
        return $member['id'];
    }

    /**
     * Show MailChimp list member
     *
     * @param string $list
     * @param string $id
     * @return array
     */
    public function showMember(string $list, string $id)
    {
        return $this->mailChimp->get("lists/$list/members/$id");
    }

    /**
     * Update MailChimp list member
     *
     * @param string $list
     * @param string $id
     * @param array $member
     * @return boolean
     */
    public function updateMember(string $list, string $id, array $member)
    {
        unset($member['id']);
        unset($member['list_id']);
        return $this->mailChimp->patch("lists/$list/members/$id", $member);
    }

    /**
     * Delete MailChimp list member
     *
     * @param string $list
     * @param string $id
     * @return boolean
     */
    public function deleteMember(string $list, string $id)
    {
        return $this->mailChimp->delete("lists/$list/members/$id");
    }
}
