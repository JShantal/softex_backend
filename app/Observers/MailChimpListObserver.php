<?php

namespace App\Observers;

use MailChimpHelper;
use App\Models\MailChimpList;

class MailChimpListObserver
{
    /**
     * Handle to the MailChimpList "creating" event to add list to MailChimp
     *
     * @param  MailChimpList $list
     * @return void
     */
    public function creating(MailChimpList $list)
    {
        $id = MailChimpHelper::createList($list->toArray());
        $list->remote_id = $id;
    }

    /**
     * Handle to the MailChimpList "updating" event to update list in MailChimp
     *
     * @param  MailChimpList $list
     * @return void
     */
    public function updating(MailChimpList $list)
    {
        MailChimpHelper::updateList($list->remote_id, $list->toArray());
    }

    /**
     * Handle to the MailChimpList "deleting" event to remove list from MailChimp
     *
     * @param  MailChimpList $list
     * @return void
     */
    public function deleting(MailChimpList $list)
    {
        MailChimpHelper::deleteList($list->remote_id);
    }
}
