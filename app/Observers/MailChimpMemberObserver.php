<?php

namespace App\Observers;

use MailChimpHelper;
use App\Models\MailChimpMember;

class MailChimpMemberObserver
{
    /**
     * Handle to the MailChimpMember "creating" event to add member to MailChimp
     *
     * @param  MailChimpMember $member
     * @return void
     */
    public function creating(MailChimpMember $member)
    {
        $data = $member->toArray();
        $id = MailChimpHelper::createMember($member->list->remote_id, $data);
        $member->remote_id = $id;
    }

    /**
     * Handle to the MailChimpMember "updating" event to update member in MailChimp
     *
     * @param  MailChimpMember $member
     * @return void
     */
    public function updating(MailChimpMember $member)
    {
        $data = $member->toArray();
        MailChimpHelper::updateMember($member->list->remote_id, $member->remote_id, $data);
    }

    /**
     * Handle to the MailChimpMember "deleting" event to remove member from MailChimp
     *
     * @param  MailChimpMember $member
     * @return void
     */
    public function deleting(MailChimpMember $member)
    {
        MailChimpHelper::deleteMember($member->list->remote_id, $member->remote_id);
    }
}
