<?php

namespace App\Http\Controllers;

use App\Models\MailChimpMember;
use App\Http\Requests\MailChimpMemberRequest;
use App\Http\Resources\MailChimpMemberResource;

class MailChimpMemberController extends Controller
{
    /**
     * Display a listing of the members.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return MailChimpMemberResource::collection(MailChimpMember::all());
    }

    /**
     * Store a newly created member in storage.
     *
     * @param MailChimpMemberRequest $request
     * @return MailChimpMemberResource
     */
    public function store(MailChimpMemberRequest $request)
    {
        return new MailChimpMemberResource(MailChimpMember::create($request->all()));
    }

    /**
     * Display the specified member.
     *
     * @param MailChimpMember $member
     * @return MailChimpMemberResource
     */
    public function show(MailChimpMember $member)
    {
        return new MailChimpMemberResource($member);
    }

    /**
     * Update the specified member in storage.
     *
     * @param MailChimpMemberRequest $request
     * @param MailChimpMember $member
     * @return MailChimpMemberResource
     */
    public function update(MailChimpMemberRequest $request, MailChimpMember $member)
    {
        $member->fill($request->all())->save();
        return new MailChimpMemberResource($member);
    }

    /**
     * Remove the specified member from storage.
     *
     * @param MailChimpMember $member
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(MailChimpMember $member)
    {
        $member->delete();
        return response()->json(null, 204);
    }
}
