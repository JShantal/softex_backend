<?php

namespace App\Http\Controllers;

use App\Models\MailChimpList;
use App\Http\Requests\MailChimpListRequest;
use App\Http\Resources\MailChimpListResource;

class MailChimpListController extends Controller
{
    /**
     * Display a listing of the lists.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return MailChimpListResource::collection(MailChimpList::all());
    }

    /**
     * Store a newly created list in storage.
     *
     * @param MailChimpListRequest $request
     * @return MailChimpListResource
     */
    public function store(MailChimpListRequest $request)
    {
        return new MailChimpListResource(MailChimpList::create($request->all()));
    }

    /**
     * Display the specified list.
     *
     * @param MailChimpList $list
     * @return MailChimpListResource
     */
    public function show(MailChimpList $list)
    {
        return new MailChimpListResource($list);

    }

    /**
     * Update the specified list in storage.
     *
     * @param MailChimpListRequest $request
     * @param MailChimpList $list
     * @return MailChimpListResource
     */
    public function update(MailChimpListRequest $request, MailChimpList $list)
    {
        $list->fill($request->all())->save();
        return new MailChimpListResource($list);
    }

    /**
     * Remove the specified list from storage.
     *
     * @param MailChimpList $list
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(MailChimpList $list)
    {
        $list->delete();
        return response()->json(null, 204);
    }
}
