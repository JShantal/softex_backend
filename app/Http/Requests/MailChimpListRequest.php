<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailChimpListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->list) {
            $required = ['bail'];
            $id = $this->list->id;
        } else {
            $required = ['bail', 'required'];
            $id = null;
        }

        return [
            'name' => array_merge($required, ['string', "unique:lists,name,$id"]),
            'contact' => array_merge($required, ['array']),
            'contact.company' => array_merge($required, ['string']),
            'contact.address1' => array_merge($required, ['string']),
            'contact.address2' => ['bail', 'string'],
            'contact.city' => array_merge($required, ['string']),
            'contact.state' => array_merge($required, ['string']),
            'contact.zip' => array_merge($required, ['string']),
            'contact.country' => array_merge($required, ['string', 'size:2']),
            'contact.phone' => ['string'],
            'permission_reminder' => array_merge($required, ['string']),
            'campaign_defaults' => array_merge($required, ['array']),
            'campaign_defaults.from_name' => array_merge($required, ['string']),
            'campaign_defaults.from_email' => array_merge($required, ['string']),
            'campaign_defaults.subject' => array_merge($required, ['string']),
            'campaign_defaults.language' => array_merge($required, ['string', 'size:2']),
            'email_type_option' => array_merge($required, ['boolean']),
        ];
    }
}
