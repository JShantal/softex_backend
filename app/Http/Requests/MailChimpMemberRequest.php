<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailChimpMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->member) {
            $required = ['bail'];
            $id = $this->member->id;
        } else {
            $required = ['bail', 'required'];
            $id = null;
        }

        return [
            'email_address' => array_merge($required, ["unique_with:members,email_address,list_id,$id"]),
            'email_type' => array_merge($required, ['in:html,text']),
            'status' => array_merge($required, ['in:subscribed,unsubscribed,cleaned,pending']),
            'language' => array_merge($required, ['string', 'size:2']),
            'list_id' => array_merge($required, ['exists:lists,id']),
        ];
    }
}
