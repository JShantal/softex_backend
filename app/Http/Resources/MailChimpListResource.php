<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MailChimpListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'contact' => $this->contact,
            'permission_reminder' => $this->permission_reminder,
            'campaign_defaults' => $this->campaign_defaults,
            'email_type_option' => $this->email_type_option,
        ];
    }
}
