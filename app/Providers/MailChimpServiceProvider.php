<?php

namespace App\Providers;

use DrewM\MailChimp\MailChimp;
use App\Helpers\MailChimpHelper;
use Illuminate\Support\ServiceProvider;

class MailChimpServiceProvider extends ServiceProvider
{
    /**
     * Register MailChimp service.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MailChimpHelper::class, function () {
            $mailChimp = new MailChimp(env('MAILCHIMP_APIKEY'));
            $mailChimp->verify_ssl = env('MAILCHIMP_SSL', true);
            return new MailChimpHelper($mailChimp);
        });
        $this->app->alias(MailChimpHelper::class, 'mailchimp_helper');
    }
}
