<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailChimpList extends Model
{
    protected $table = 'lists';

    protected $casts = [
        'contact' => 'array',
        'campaign_defaults' => 'array',
        'email_type_option' => 'boolean',
    ];

    protected $fillable = [
        'name',
        'contact',
        'permission_reminder',
        'campaign_defaults',
        'email_type_option',
    ];

    /**
     * Make array fields fillable
     *
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes)
    {
        foreach ($this->casts as $key => $value) {
            if ($value == 'array' && !empty($this->$key) && !empty($attributes[$key])) {
                $attributes[$key] = array_replace_recursive($this->$key, $attributes[$key]);
            }
        }
        return parent::fill($attributes);
    }

    public function members()
    {
        return $this->hasMany(MailChimpMember::class, 'list_id');
    }
}
