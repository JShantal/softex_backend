<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailChimpMember extends Model
{
    protected $table = 'members';

    protected $fillable = [
        'remote_id',
        'email_address',
        'email_type',
        'status',
        'language',
        'list_id',
    ];

    public function list()
    {
        return $this->belongsTo(MailChimpList::class, 'list_id');
    }
}
