<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('remote_id');
            $table->string('email_address');
            $table->enum('email_type', ['html', 'text'])->default('html');
            $table->enum('status', ['subscribed', 'unsubscribed', 'cleaned', 'pending'])->default('subscribed');
            $table->string('language', 2)->default('en');
            $table->unsignedInteger('list_id');
            $table->foreign('list_id')
                ->references('id')->on('lists')
                ->onDelete('cascade');
            $table->unique(['email_address', 'list_id']);
            $table->timestamps();
        });
    }
}
