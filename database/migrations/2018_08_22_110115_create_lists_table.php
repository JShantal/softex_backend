<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('remote_id');
            $table->string('name')->unique();
            $table->json('contact');
            $table->string('permission_reminder');
            $table->json('campaign_defaults');
            $table->boolean('email_type_option')->default(true);
            $table->timestamps();
        });
    }
}
