<?php

use App\Models\MailChimpList;
use App\Models\MailChimpMember;
use Illuminate\Database\Seeder;

class MailChimpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MailChimpList::flushEventListeners();
        MailChimpMember::flushEventListeners();

        $lists = MailChimpHelper::getLists();
        foreach ($lists['lists'] as $list) {
            $list = MailChimpList::create([
                'remote_id' =>  $list['id'],
                'name' =>  $list['name'],
                'contact' =>  $list['contact'],
                'permission_reminder' =>  $list['permission_reminder'],
                'campaign_defaults' =>  $list['campaign_defaults'],
                'email_type_option' =>  $list['email_type_option']
            ]);

            $members = MailChimpHelper::getMembers($list->remote_id);
            foreach ($members['members'] as $member) {
                MailChimpMember::create([
                    'remote_id' => $member['id'],
                    'email_address' => $member['email_address'],
                    'email_type' => $member['email_type'],
                    'status' => $member['status'],
                    'language' => $member['language'],
                    'list_id' => $list->id
                ]);
            }
        }
    }
}
